import { Component } from '@angular/core';
import * as $ from 'jquery';
import "../../node_modules/bootstrap/dist/js/bootstrap.min.js"
import "../../node_modules/popper.js/dist/umd/popper.min.js"
import "../../node_modules/swipebox/lib/jquery-2.1.0.min.js"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  public ngOnInit() {
    $(document).ready(() => {
      $("#gotoAbout").click(event => {
        event.preventDefault();
        $("html, body").animate({
          scrollTop: $('#about').offset().top}, 500);
      });
      $("#gotoEducation").click(event => {
        event.preventDefault();
        $("html, body").animate({
          scrollTop: $('#education').offset().top}, 1000);
      });
      $("#gotoSkill").click(event => {
        event.preventDefault();
        $("html, body").animate({
          scrollTop: $('#skill').offset().top}, 1000);
      });
      $("#gotoProject").click(event => {
        event.preventDefault();
        $("html, body").animate({
          scrollTop: $('#project').offset().top}, 1000);
      });
      $("#gotoContact").click(event => {
        event.preventDefault();
        $("html, body").animate({
          scrollTop: $('#contact').offset().top}, 1000);
      });
      $(".swipebox").click(e => {
        $('#imagepreview').attr('src', e.currentTarget.firstElementChild.src);
        $('#imagemodal').modal('show');
      });
    });
  }
}
